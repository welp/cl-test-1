(require 'asdf)

(in-package #:cl-test-1)

(defun nice ()
  "Just print the version."
  (slot-value (asdf:find-system 'cl-test-1) 'asdf:version))
