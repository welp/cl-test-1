(require 'asdf)

(asdf:defsystem #:cl-test-1
  :description "Test package for Qi"
  :license "Public Domain"
  :version "0.0.2"
  :components ((:file "main")))

(defpackage #:cl-test-1
  (:use #:cl)
  (:export #:nice))
